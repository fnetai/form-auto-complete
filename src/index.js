import React from 'react';
import Layout from "@fnet/react-layout-asya";
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';

export default (props) => {

    const form = props.form || {};

    const [value, setValue] = React.useState(props.input?.value || null);
    const [options, setOptions] = React.useState(props.input?.options || []);

    const [inputValue, setInputValue] = React.useState(props.input?.input?.value || '');

    const getOptionKey = props.input?.getOptionKey || (x => x.key || x.id || x);
    const getOptionLabel = props.input?.getOptionLabel || (x => x.label || x.title || x);

    const handleValueChange = React.useCallback((event, newValue) => {

        setValue(newValue);

        if (props.input?.onChange) props.input.onChange({ value: newValue });
    });

    const handleInputValueChange = React.useCallback((event, newValue) => {

        setInputValue(newValue);

        if (props.input?.input?.onChange) props.input.input.onChange({ value: newValue });
    });

    React.useEffect(() => {
        // form exports
        form.setValue = value => setValue(value);
        form.setOptions = value => setOptions([...value]);

        form.setInputValue = value => setInputValue(value);
    }, []);

    return (
        <Layout {...props}>

            <Autocomplete
                fullWidth
                multiple={props.input?.multiple || false}
                limitTags={props.input?.limitTags || 3}
                disablePortal={props.input?.disablePortal || false}
                freeSolo={props.input?.freeSolo || false}

                getOptionLabel={props.input?.getOptionLabel}
                isOptionEqualToValue={props.input?.isOptionEqualToValue}
                filterOptions={props.input?.filterOptions}
                getLimitTagsText={props.input?.getLimitTagsText}
                getOptionDisabled={props.input?.getOptionDisabled}
                groupBy={props.input?.groupBy}
                includeInputInList={props.input?.includeInputInList}
                loading={props.input?.loading || false}
                readOnly={props.input?.readOnly || false}
                size={props.input?.size}

                value={value}
                options={options}
                onChange={handleValueChange}

                inputValue={inputValue}
                onInputChange={handleInputValueChange}

                sx={{}}

                renderInput={params =>
                    <TextField
                        {...params}
                        required={props.input?.required || false}
                        autoFocus={props.input?.autoFocus || false}
                        placeholder={props.input?.placeholder}
                        sx={{
                            backgroundColor: "rgba(255,255,255,0.5)",
                            borderRadius: "4px",
                        }}
                    />
                }

                renderOption={(props, option) => {
                    return (
                        <li {...props} key={getOptionKey(option)}>
                            {getOptionLabel(option)}
                        </li>
                    );
                }}
            />
        </Layout>
    );
}